import { TODO_FETCH_NOTES } from "../../types/index";

const fetchNotesWatcher = notes => {
  console.log('inside todo action')
  return {
    type: TODO_FETCH_NOTES.FETCH_NOTES_REQUEST,
    notes
  };
};

export default fetchNotesWatcher;

// export const fetchNotesRequest = () =>{
//     return {
//         type : TODO_FETCH_NOTES.FETCH_NOTES_REQUEST
//     }
// }

// export const fetchNotesSuccess = notes =>{
//     return {
//         type : TODO_FETCH_NOTES.FETCH_NOTES_SUCCESS,
//         payload : notes
//     }
// }

// export const fetchNotesFailure = error =>{
//     return {
//         type : TODO_FETCH_NOTES.FETCH_NOTES_FAILURE,
//         payload : error
//     }
// }
