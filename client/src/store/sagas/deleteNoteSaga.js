import { call,takeLatest } from 'redux-saga/effects'
import { TODO_DELETE_NOTE } from '../../types/index';
import { deleteCall } from '../../utils/apiSignature';


function* deleteNoteWorker(payload) {
    console.log('id',payload.id);
    const { response, error } = yield call(
      deleteCall,
      `https://localhost:8001/todos/${payload.id} `,
    );
    if (response) {
    //   yield put({ type: types.COMPANY_CLIENT.LIST });
    //   const data = {
    //     msg: 'Client Deleted !!!',
    //     flagType: 'success',
    //   };
    //   yield put({ type: types.SHOW_ERROR, data });
    } else if (error) {
    //   const data = {
    //     msg: error.response.data.errorMessage,
    //     flagType: 'error',
    //   };
    //   yield put({ type: types.SHOW_ERROR, data });
    }
  }

export default function* deleteNoteWatcher() {
  console.log("inside saga")
    yield takeLatest(TODO_DELETE_NOTE.DELETE_NOTE_REQUEST, deleteNoteWorker);
  }