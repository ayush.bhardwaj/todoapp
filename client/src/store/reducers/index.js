import { combineReducers } from 'redux';
import fetchNotesReducer from './fetchNotesReducer';
import addNoteReducer from './addNoteReducer';
import deleteNoteReducer from './deleteNoteReducer';
import { reducer as formReducer } from 'redux-form';


const rootReducer = combineReducers({
  form : formReducer,
  fetchNotesReducer,
  addNoteReducer,
  deleteNoteReducer,
});

export default rootReducer;
