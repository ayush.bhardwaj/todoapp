import React from 'react';
import { TextField } from '@material-ui/core';
import PropTypes from 'prop-types';

const RenderField = ({
  input,
  label,
  type,
  disabled,
  upperCase,
  maxLength,
  meta: { touched, error },
}) => {
  return (
    <TextField
      error={touched && error}
      // eslint-disable-next-line react/jsx-props-no-spreading
      {...input}
      type={type}
      label={label}
      fullWidth
      margin="normal"
      disabled={disabled}
      className={upperCase && 'text-upper-case'}
      inputProps={{ maxLength }}
    />
  );
};

export default RenderField;

RenderField.propTypes = {
  input: PropTypes.shape({
    name: PropTypes.string,
    value: PropTypes.any,
  }).isRequired,
  label: PropTypes.string.isRequired,
  type: PropTypes.string.isRequired,
  upperCase: PropTypes.bool,
  maxLength: PropTypes.number,
  meta: PropTypes.shape({
    touched: PropTypes.bool,
    error: PropTypes.string,
    active: PropTypes.bool,
    // asyncValidating: PropTypes.bool,
    // autofilled: PropTypes.bool,
    // dirty: PropTypes.bool,
    // invalid: PropTypes.bool,
    // pristine: PropTypes.bool,
    // submitting: PropTypes.bool,
    // submitFailed: PropTypes.bool,
    // valid: PropTypes.bool,
    // visited: PropTypes.bool,
  }).isRequired,
  disabled: PropTypes.bool,
};

RenderField.defaultProps = {
  disabled: false,
  maxLength: 75,
  upperCase: false,
};
