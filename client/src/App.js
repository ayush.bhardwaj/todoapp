import React from 'react';
import Typography from '@material-ui/core/Typography';
import './App.css';
import TodoForm from './components/TodoForm';
import { Provider } from 'react-redux';
import store  from './store/index';


function App() {
  return (
    <Provider store = {store}>
    <div className="App">
      <Typography component="h1" variant="h2">
        Todos
      </Typography>

      <TodoForm/>
    </div>
    </Provider>
  );
}

export default App;
